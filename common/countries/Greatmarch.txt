#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 82  130  152 }

revolutionary_colors = { 82  130  152 }

historical_idea_groups = {
	exploration_ideas
	maritime_ideas
	economic_ideas
	offensive_ideas
	expansion_ideas
	quality_ideas	
	administrative_ideas	
	influence_ideas
}

historical_units = {
	cannorian_1_medieval_infantry
	cannorian_1_medieval_knights
	cannorian_5_men_at_arms
	cannorian_9_pike_and_shot
	cannorian_10_gun_knights
	cannorian_12_thorn_formation
	cannorian_14_outriders
	cannorian_15_volley_infantry
	cannorian_18_reformed_outriders
	cannorian_19_thorn_formation2
	cannorian_23_line_infantry
	cannorian_26_carabiners
	cannorian_26_rosecoat_infantry
	cannorian_28_impulse_infantry
	cannorian_30_cuirassier
	cannorian_30_drill_infantry
}

monarch_names = {
	"Acromar #0" = 1
	"Adelar #0" = 1
	"Alain #0" = 1
	"Alen #0" = 1
	"Arnold #0" = 1
	"Camir #0" = 1
	"Camor #0" = 1
	"Canrec #0" = 1
	"Carlan #0" = 1
	"Celgal #0" = 1
	"Ciramod #0" = 1
	"Clarimond #0" = 1
	"Clothar #0" = 1
	"Colyn #0" = 1
	"Coreg #0" = 1
	"Crovan #0" = 1
	"Crovis #0" = 1
	"Corac #0" = 1
	"Corric #0" = 1
	"Dalyon #0" = 1
	"Delia #0" = 1
	"Delian #0" = 1
	"Devac #0" = 1
	"Edmund #0" = 1
	"Frederic #0" = 1
	"Godrac #0" = 1
	"Godric #0" = 1
	"Godryc #0" = 1
	"Godwin #0" = 1
	"Gracos #0" = 1
	"Henric #0" = 1
	"Humac #0" = 1
	"Humban #0" = 1
	"Humbar #0" = 1
	"Humbert #0" = 1
	"Lain #0" = 1
	"Lan #0" = 1
	"Madalac #0" = 1
	"Marcan #0" = 1
	"Ottrac #0" = 1
	"Ottran #0" = 1
	"Petran #0" = 1
	"Rabac #0" = 1
	"Rycan #0" = 1
	"Rogec #0" = 1
	"Stovac #0" = 1
	"Stovan #0" = 1
	"Teagan #0" = 1
	"Tomac #0" = 1
	"Toman #0" = 1
	"Tomar #0" = 1
	"Tormac #0" = 1
	"Ulric #0" = 1
	"Venac #0" = 1
	"Vencan #0" = 1
	"Bellac #0" = 1
	

	"Adela #0" = -10
	"Auci #0" = -10
	"Bella #0" = -10
	"Catherine #0" = -10
	"Clarimonde #0" = -10
	"Clarya #0" = -10
	"Clothilde #0" = -10
	"Constance #0" = -10
	"Cora #0" = -10
	"Coralinde #0" = -10
	"Dalya #0" = -10
	"Emma #0" = -10
	"Etta #0" = -10
	"Humba #0" = -10
	"Lisban #0" = -10
	"Madala #0" = -10
	"Magda #0" = -10
	"Robyn #0" = -10
}

leader_names = {
	"of Greatmarch" "of the Wood" Woodling Lumberton "of Alenvord" "of Wolfden" Wolfslayer Wolfkin Wolfhunter "of Legion's Clearing" "of Aldtempel" "of Gardfort" "of Mossford" Mossing
	"of Jonsway" "of the Alen" Alen Alenman
}

ship_names = {
	#Generic Cannorian
	Adamant Advantage Adventure Advice Answer Ardent Armada Arrogant Assistance Association Assurance Atlas Audacious 
	Bear Beaver "Black Galley" "Black Pinnace" "Black Prince" "Black Blood" Captain Centurion Coronation Courage Crocodile Crown
	Daisy Defence Defiance Devastation Diamond Director Dolphin Dragon Drake Dreadnaught Duke Duchess Eagle Elephant Excellent Exchange Expedition Experiment
	Falcon Fame Favourite Fellowship Fleetwood Flight Flirt Formidable Forrester Fortitude Fortune Gillyflower Globe "Golden Horse" "Golden Lion" "Golden Phoenix" Goliath Goodgrace Governor
	"Grand Mistress" "Great Bark" "Great Charity" "Great Galley" "Great Gift"  "Great Pinnace" "Great Zebra" "Green Dragon" Greyhound Griffin Guide "Half Moon" Hare Harpy Hawke Hazardous Heart
	Hero Hope "Hope Bark" "Hopeful Adventure" Humble Hunter Illustrious Impregnable Increase Indefatigable Inflexible Intrepid Invincible "Less Bark" "Less Pinnace" "Less Zebra"
	Lively Lizard Lion Magnanime Magnificent Majestic Makeshift Medusa Minotaur Moderate Monarch Moon Moor "New Bark" Ocean 
	Pansy Panther Parrot Porcupine Powerful President Prince "Prince Consort" "Prince Royal"
	Redoubtable Reformation Regent Renown Repulse Research Reserve Resistance Resolution Restoration Revenge 
	Salamander Sandwich Sapphire Satisfaction Seahorse Search Sheerness Speaker Speedwell Sphinx Splendid Sprite Stag Standard Stately Success Sunlight Sunbeam Superb Swan Sweepstake Swift Swiftsure
	Terrible Terror Thunderer Tiger Tremendous Trident Trinity Triumph Trusty
	Unicorn Union Unity Valiant Vanguard Venerable Vestal Veteran Victor Vindictive Virtue Violet
	Warrior Warspite "Young Prince" Zealous

	Survey Surveillance Cow Ox Bird Hawk Sovereign Emperor Count Lord Baron

	Knight Paladin Dragonslayer Cleric Rogue Fighter Ranger Sorcerer Wizard Warlock Monk Druid
	Strength Dexterity Constitution Intelligence Wisdom Charisma
	
	#Regent Court Deities
	Castellos Dame "The Dame" Halanna Ysh Yshtralania Agradls Adean Esmaryal Ryala Edronias Falah Nerat Ara Minara Munas Moonsinger Nathalyne Begga Corin Balgar
	Uelos Drax'os
}

army_names = {
	"Greatmarch Army" "Army $PROVINCE$"
}

fleet_names = {
	"Greatmarcher Fleet" "Bay of Chills Squadron" "Giant's Grave Squadron" "Ice Squadron" "Lumber Squadron"
}