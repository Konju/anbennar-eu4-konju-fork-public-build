estate_artificers_disaster = {
	potential = {
		has_dlc = "The Cossacks"
		has_estate = estate_artificers
		estate_influence = {
			estate = estate_artificers
			influence = 60
		}
	}


	can_start = {
		has_any_disaster = no
		estate_influence = {
			estate = estate_artificers
			influence = 80
		}
	}
	
	can_stop = {
		OR = {
			has_any_disaster = yes
			NOT = {
				estate_influence = {
					estate = estate_artificers
					influence = 80
				}		
			}
		}
	}
	
	down_progress = {
		factor = 1
	}
	
	progress = {
		modifier = {
			factor = 1
			estate_influence = {
				estate = estate_artificers
				influence = 80
			}
			hidden_trigger = {
				NOT = {
					estate_influence = {
						estate = estate_artificers
						influence = 85
					}			
				}			
			}
		}
		modifier = {
			factor = 2
			estate_influence = {
				estate = estate_artificers
				influence = 85
			}
			hidden_trigger = {
				NOT = {
					estate_influence = {
						estate = estate_artificers
						influence = 90
					}			
				}			
			}
		}
		modifier = {
			factor = 3
			estate_influence = {
				estate = estate_artificers
				influence = 90
			}
			hidden_trigger = {
				NOT = {
					estate_influence = {
						estate = estate_artificers
						influence = 95
					}			
				}			
			}
		}	
		modifier = {
			factor = 4
			estate_influence = {
				estate = estate_artificers
				influence = 95
			}
			hidden_trigger = {
				NOT = {
					estate_influence = {
						estate = estate_artificers
						influence = 100
					}			
				}			
			}
		}	
		modifier = {
			factor = 5
			estate_influence = {
				estate = estate_artificers
				influence = 100
			}
		}		
	}
	
	can_end = {
		custom_trigger_tooltip = {
			tooltip = EST_CRUSHED_ARTIFICERS
			OR = {
				NOT = { has_country_flag = artificers_estate_in_power }
				has_country_flag = noble_estate_in_power
				has_country_flag = church_estate_in_power
				has_country_flag = burghers_estate_in_power
			}						
		}
	}
	
	modifier = {	
		global_manpower_modifier = -0.20
		land_forcelimit_modifier = -0.20
		global_tax_modifier = -0.20
		prestige = -0.5
	}

	on_start = estate_disasters.11
	on_end = estate_disasters.12
	
	on_monthly = {
	}
}

