###########################################################
# Events for the Adventurer Estate
#
# written by Tus3
###########################################################

namespace = adventurer_estate_events


#Advancement of the Adventurers
country_event = {
	id = adventurer_estate_events.1
	title = adventurer_estate_events.1.t
	desc = adventurer_estate_events.1.d
	picture = COSSACK_ESTATE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		any_owned_province = {
			has_estate = estate_adventurers
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURERS_ADVANCES
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURERS_ADVANCES_20
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURERS_DECLINES
			}
		}
	}
	
	mean_time_to_happen = {
		days = 1
	}
	

	option = {
		name = adventurer_estate_events.1.a #Ok
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_ADVENTURERS_ADVANCE
			influence = 10
			duration = 5475
		}
	}
}

#Decline of Adventurer Power
country_event = {
	id = adventurer_estate_events.2
	title = adventurer_estate_events.2.t
	desc = adventurer_estate_events.2.d
	picture = COSSACK_ESTATE_eventPicture
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		any_owned_province = {
			has_estate = estate_adventurers
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURERS_ADVANCES
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURERS_ADVANCES_20
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURERS_DECLINES
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = adventurer_estate_events.2.a #Ok
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_ADVENTURERS_DECLINES
			influence = -10
			duration = 5475
		}
	}
}

#Military Commisions
#Adventurers vs Nobles
country_event = {
	id = adventurer_estate_events.3
	title = adventurer_estate_events.3.t
	desc = adventurer_estate_events.3.d
	picture = COSSACK_ESTATE_eventPicture
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		has_estate = estate_nobles
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_NO_COMMISSIONS_FOR_ADVENTURERS
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_COMMISSIONS_FOR_ADVENTURERS
			}
		}
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	is_triggered_only = yes
	
	
	
	option = {
		name = adventurer_estate_events.3.a #Ok.
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				NOT = {
					estate_loyalty = {
						estate = estate_adventurers
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_adventurers
					influence = 60
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_nobles
					influence = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = -15
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_NO_COMMISSIONS_FOR_ADVENTURERS
			influence = -10
			duration = 3650
		}
		add_estate_influence_modifier = {
			estate = estate_nobles
			desc = EST_VAL_NO_COMMISSIONS_FOR_ADVENTURERS
			influence = 10
			duration = 3650
		}
		add_army_tradition = -5
	}
	option = {
		name = adventurer_estate_events.3.b #We promote on merit.
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.2
				NOT = {
					estate_loyalty = {
						estate = estate_nobles
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_adventurers
					influence = 60
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_nobles
					influence = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = -15
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_COMMISSIONS_FOR_ADVENTURERS
			influence = 10
			duration = 3650
		}
		add_estate_influence_modifier = {
			estate = estate_nobles
			desc = EST_VAL_COMMISSIONS_FOR_ADVENTURERS
			influence = -10
			duration = 3650
		}
	}
}

#Peasant flight
#Adventurers vs Nobles
country_event = {
	id = adventurer_estate_events.4
	title = adventurer_estate_events.4.t
	desc = adventurer_estate_events.4.d
	picture = COSSACK_ESTATE_eventPicture
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		estate_influence = {
			estate = estate_nobles
			influence = 40
		}
		estate_influence = {
			estate = estate_adventurers
			influence = 25
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_SERFS_HUNTED
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_SERFS_TO_COSSACKS
			}
		}
		any_owned_province = {
			has_estate = estate_nobles
		}
		any_owned_province = {
			has_estate = estate_adventurers
		}
	}
	
	mean_time_to_happen = {
		days = 1
	}
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			random_owned_province = {
				limit = {
					has_estate = estate_nobles
				}
				set_province_flag = serfs_are_fleeing
			}
			random_owned_province = {
				limit = {
					has_estate = estate_adventurers
				}
				set_province_flag = serfs_are_arriving
			}
		}
	}
	
	option = {
		name = adventurer_estate_events.4.a #These Serfs must be brought back.
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				NOT = {
					estate_loyalty = {
						estate = estate_adventurers
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_adventurers
					influence = 60
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_nobles
					influence = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = -15
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_SERFS_HUNTED
			influence = -10
			duration = 3650
		}
		add_estate_influence_modifier = {
			estate = estate_nobles
			desc = EST_VAL_SERFS_BROUGHT_BACK
			influence = 10
			duration = 3650
		}
		random_owned_province = {
			limit = {
				has_province_flag = serfs_are_fleeing
			}
			add_base_production = 1
			clr_province_flag = serfs_are_fleeing
		}
		hidden_effect = {
			random_owned_province = {
				limit = {
					has_province_flag = serfs_are_arriving
				}
				clr_province_flag = serfs_are_arriving
			}
		}
	}
	option = {
		name = adventurer_estate_events.4.b #We cannot afford to anger the Adventurers.
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.2
				NOT = {
					estate_loyalty = {
						estate = estate_nobles
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_adventurers
					influence = 60
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_nobles
					influence = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = -15
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_SERFS_TO_COSSACKS
			influence = 10
			duration = 3650
		}
		add_estate_influence_modifier = {
			estate = estate_nobles
			desc = EST_VAL_SERFS_DEFECTING
			influence = -10
			duration = 3650
		}
		random_owned_province = {
			limit = {
				has_province_flag = serfs_are_arriving
			}
			add_base_manpower = 1
			clr_province_flag = serfs_are_arriving
		}
		hidden_effect = {
			random_owned_province = {
				limit = {
					has_province_flag = serfs_are_fleeing
				}
				clr_province_flag = serfs_are_fleeing
			}
		}
	}
}

#Adventurer Raiders
country_event = {
	id = adventurer_estate_events.5
	title = adventurer_estate_events.5.t
	desc = adventurer_estate_events.5.d
	picture = COSSACK_ESTATE_RAIDERS_eventPicture
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		has_estate = estate_burghers
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_SUPPORTED_ADVENTURERS
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_SUPPORTED_BURGHERS_VS_ADVENTURERS
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = adventurer_estate_events.5.a #Support the $ESTATE_ADVENTURERS$.
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				NOT = {
					estate_loyalty = {
						estate = estate_burghers
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_adventurers
					influence = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = 15
		}
		add_estate_loyalty = {
			estate = estate_burghers
			loyalty = -15
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_SUPPORTED_ADVENTURERS
			influence = 10
			duration = 5475
		}
		add_estate_influence_modifier = {
			estate = estate_burghers
			desc = EST_VAL_SUPPORTED_ADVENTURERS
			influence = -10
			duration = 5475
		}
	}
	option = {
		name = adventurer_estate_events.5.b #Supports the $ESTATE_BURGHERS$.
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				NOT = {
					estate_loyalty = {
						estate = estate_adventurers
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_burghers
					influence = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = -15
		}
		add_estate_loyalty = {
			estate = estate_burghers
			loyalty = 15
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_SUPPORTED_BURGHERS_VS_ADVENTURERS
			influence = -10
			duration = 5475
		}
		add_estate_influence_modifier = {
			estate = estate_burghers
			desc = EST_VAL_SUPPORTED_BURGHERS_VS_ADVENTURERS
			influence = 10
			duration = 5475
		}
	}
}

#Petition for Recognition/Privileges
#More Registered Adventurers
country_event = {
	id = adventurer_estate_events.6
	title = adventurer_estate_events.6.t
	desc = adventurer_estate_events.6.d
	picture = COSSACK_ESTATE_eventPicture
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		NOT = { manpower_percentage = 0.135 }
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_REGISTRATED_ADVENTURERS
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_DENIED_ADVENTURER_REGISTRATION
			}
		}
		any_owned_province = {
			has_estate = estate_adventurers
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = adventurer_estate_events.6.a #We need more registered Adventurers!
		add_yearly_manpower = 1
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				NOT = {
					estate_loyalty = {
						estate = estate_adventurers
						loyalty = 40
					}
				}
				modifier = {
					factor = 1.5
					NOT = { manpower_percentage = 0.4 }
				}
				modifier = {
					factor = 0.5
					manpower_percentage = 0.6
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_adventurers
					influence = 60
				}
			}
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_REGISTRATED_ADVENTURERS
			influence = 10
			duration = 5475
		}
	}
	option = {
		name = adventurer_estate_events.6.b #
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				NOT = {
					estate_loyalty = {
						estate = estate_adventurers
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_adventurers
					influence = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = -15
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_DENIED_ADVENTURER_REGISTRATION
			influence = -10
			duration = 5475
		}
	}
}

#Settling New Lands
country_event = {
	id = adventurer_estate_events.7
	title = adventurer_estate_events.7.t
	desc = adventurer_estate_events.7.d
	picture = COSSACK_ESTATE_eventPicture
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		num_of_colonists = 1
		any_owned_province = {
			superregion = escann_superregion
			NOT = { has_estate = yes }
			NOT = { development = 10 }
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURER_SETTLERS
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_NO_ADVENTURER_SETTLERS
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = adventurer_estate_events.7.a #Yes, we can use more $ESTATE_ADVENTURERS$ on the frontier.
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				NOT = {
					estate_loyalty = {
						estate = estate_adventurers
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_adventurers
					influence = 60
				}
			}
		}
		add_country_modifier = {
			name = "adventurer_settlers"
			duration = 5475
		}
		random_owned_province = {
			limit = {
				NOT = { development = 10 }
				NOT = { has_estate = yes }
				superregion = escann_superregion
				is_territory = no
			}
			set_estate = estate_adventurers
		}
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = 15
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_ADVENTURER_SETTLERS
			influence = 10
			duration = 5475
		}
	}
	option = {
		name = adventurer_estate_events.7.b #No, we cannot trust those oppurtunists
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				NOT = {
					estate_loyalty = {
						estate = estate_adventurers
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_adventurers
					influence = 60
				}
			}
		}
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = -15
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_NO_ADVENTURER_SETTLERS
			influence = -10
			duration = 5475
		}
	}
}

#Adventurers in financial trouble
country_event = {
	id = adventurer_estate_events.8
	title = adventurer_estate_events.8.t
	desc = adventurer_estate_events.8.d
	picture = COSSACK_ESTATE_eventPicture
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		any_owned_province = {
			NOT = { development = 10 }
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_SUPPLIED_ADVENTURERS
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURERS_LEAVING
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = adventurer_estate_events.8.a #Give them contracts for unimportant stuff
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				NOT = {
					estate_loyalty = {
						estate = estate_adventurers
						loyalty = 40
					}
				}
			}
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_adventurers
					influence = 60
				}
			}
		}
		add_years_of_income = -0.25
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = 10
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_SUPPLIED_ADVENTURERS
			influence = 10
			duration = 5475
		}
	}
	option = {
		name = adventurer_estate_events.8.b #Let them leave
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_adventurers
					influence = 60
				}
			}
		}
		add_yearly_manpower = -0.25
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_ADVENTURERS_LEAVING
			influence = -10
			duration = 5475
		}
	}
}

#Complaining Adventurers
country_event = {
	id = adventurer_estate_events.9
	title = adventurer_estate_events.9.t
	desc = adventurer_estate_events.9.d
	picture = COSSACK_ESTATE_UPSET_eventPicture
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		NOT = {
			estate_loyalty = {
				estate = estate_adventurers
				loyalty = 45
			}
		}
		any_owned_province = {
			has_estate = estate_adventurers
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURERS_INDEPENDENCE
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURERS_LEAVING
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = adventurer_estate_events.9.a #
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				NOT = {
					estate_loyalty = {
						estate = estate_adventurers
						loyalty = 35
					}
				}
			}
			modifier = {
				factor = 0.5
				estate_influence = {
					estate = estate_adventurers
					influence = 60
				}
			}
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_ADVENTURERS_INDEPENDENCE
			influence = 10
			duration = 7300
		}
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = 10
		}
		every_owned_province = {
			limit = {
				has_estate = estate_adventurers
			}
			add_local_autonomy = 33
		}
	}
	option = {
		name = adventurer_estate_events.9.b #
		ai_chance = {
			factor = 50
			modifier = {
				factor = 1.5
				estate_influence = {
					estate = estate_adventurers
					influence = 70
				}
			}
			modifier = {
				factor = 0
				NOT = { manpower_percentage = 0.8 }
			}
		}
		add_yearly_manpower = -0.25
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_ADVENTURERS_LEAVING
			influence = -10
			duration = 5475
		}
	}
}

#Illegal settlements
country_event = {
	id = adventurer_estate_events.10
	title = adventurer_estate_events.10.t
	desc = adventurer_estate_events.10.d
	picture = COSSACK_ESTATE_eventPicture
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		any_owned_province = {
			NOT = { development = 10 }
			is_capital = no
			is_overseas = no
			NOT = { has_estate = yes }
			is_territory = no
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	immediate = {
		hidden_effect = {
			random_owned_province = {
				limit = {
					NOT = { development = 10 }
					is_capital = no
					is_overseas = no
					NOT = { has_estate = yes }
					is_territory = no
				}
				set_province_flag = adventurers_settling
			}
		}
	}
	
	option = {
		name = adventurer_estate_events.10.a #
		random_owned_province = {
			limit = {
				has_province_flag = adventurers_settling
			}
			set_estate = estate_adventurers
			add_base_manpower = 1
			clr_province_flag = adventurers_settling
		}
	}
	option = {
		name = adventurer_estate_events.10.b # Reverse this development
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = -10
		}
		random_owned_province = {
			limit = {
				has_province_flag = adventurers_settling
			}
			clr_province_flag = adventurers_settling
			random_list =  {
				25 = {
					spawn_rebels = {
						size = 1
						type = anti_tax_rebels
					}
				}
				75 = { }
			}
		}
	}
}

#Demands at high influence
#Want to lead own troops
country_event = {
	id = adventurer_estate_events.11
	title = adventurer_estate_events.11.t
	desc = adventurer_estate_events.11.d
	picture = COSSACK_ESTATE_UPSET_eventPicture
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		any_owned_province = {
			has_estate = estate_adventurers
		}
		estate_influence = {
			estate = estate_adventurers
			influence = 70
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURERS_LEADING_OWN_TROOPS
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURER_DEMANDS_REBUFFED
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = adventurer_estate_events.11.a #Preposterous.
		ai_chance = {
			factor = 1
		}
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = -20
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_ADVENTURER_DEMANDS_REBUFFED
			influence = -10
			duration = 3650
		}
	}
	option = {
		name = adventurer_estate_events.11.b #Give in to their demands.
		ai_chance = {
			factor = 0
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_ADVENTURERS_LEADING_OWN_TROOPS
			influence = 10
			duration = 7300
		}
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = 10
		}
		add_estate_loyalty = {
			estate = estate_nobles
			loyalty = -5
		}
		add_country_modifier = {
			name = "adventurers_leading_own_troops"
			duration = 7300
		}
	}
}

#Adventurers demand more autonomy
country_event = {
	id = adventurer_estate_events.12
	title = adventurer_estate_events.12.t
	desc = adventurer_estate_events.12.d
	picture = COSSACK_ESTATE_UPSET_eventPicture
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		estate_influence = {
			estate = estate_adventurers
			influence = 70
		}
		any_owned_province = {
			has_estate = estate_adventurers
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURERS_INDEPENDENCE
			}
		}
		NOT = {
			has_estate_influence_modifier = {
				estate = estate_adventurers
				modifier = EST_VAL_ADVENTURER_DEMANDS_REBUFFED
			}
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = adventurer_estate_events.12.a #
		ai_chance = {
			factor = 0
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_ADVENTURERS_INDEPENDENCE
			influence = 10
			duration = 7300
		}
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = 10
		}
		every_owned_province = {
			limit = {
				has_estate = estate_adventurers
			}
			add_local_autonomy = 33
		}
	}
	option = {
		name = adventurer_estate_events.12.b #
		ai_chance = {
			factor = 1
		}
		add_estate_influence_modifier = {
			estate = estate_adventurers
			desc = EST_VAL_ADVENTURER_DEMANDS_REBUFFED
			influence = -10
			duration = 5475
		}
		add_estate_loyalty = {
			estate = estate_adventurers
			loyalty = -15
		}
	}
}

#Nice things can also come from strong and loyal Adventurers.
#Bonus for high influence + high loyalty
country_event = {
	id = adventurer_estate_events.13
	title = adventurer_estate_events.13.t
	desc = adventurer_estate_events.13.d
	picture = BURGHER_ESTATE_eventPicture
	
	trigger = {
		has_dlc = "The Cossacks"
		has_estate = estate_adventurers
		any_owned_province = {
			has_estate = estate_adventurers
		}
		estate_influence = {
			estate = estate_adventurers
			influence = 60
		}
		estate_loyalty = {
			estate = estate_adventurers
			loyalty = 60
		}
	}
	
	is_triggered_only = yes
	
	mean_time_to_happen = {
		days = 1
	}
	
	option = {
		name = adventurer_estate_events.13.a #
		add_army_tradition = 5
		add_yearly_manpower = 0.5
	}
}

#Local Revolt
province_event = {
	id = adventurer_estate_events.15
	title = adventurer_estate_events.15.t
	desc = adventurer_estate_events.15.d
	picture = COSSACK_ESTATE_UPSET_eventPicture
	
	is_triggered_only = yes

	option = {
		name = adventurer_estate_events.15.a #Ok.
		
	}
}