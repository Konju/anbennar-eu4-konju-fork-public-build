country_decisions = {

	gnomish_nation = {
		major = yes
		potential = {
			normal_or_historical_nations = yes
			NOT = { has_country_flag = formed_gnomish_hierarchy_flag }
			#NOT = { tag = A80 } #You're not Iochand
			NOT = { exists = A79 } #Gnomish Hierarchy doesn't exist
			OR = {
				ai = no
				is_playing_custom_nation = no
			}
			culture_group = gnomish
			OR = {
				ai = no
				AND = {
					ai = yes
					num_of_cities = 3
				}
			}
			is_colonial_nation = no
			OR = {
				is_former_colonial_nation = no
				AND = {
					is_former_colonial_nation = yes
					ai = no
				}
			}
		}
		allow = {
			#adm_tech = 10
			is_free_or_tributary_trigger = yes
			is_nomad = no
			is_at_war = no
			owns_core_province = 177		# Oddansbay
			owns_core_province = 167		# Royvibobb
			owns_core_province = 172		# Wessex
			owns_core_province = 173		# Lothian
		}
		effect = {
			177 = {	#Move capital to Oddansbay
				move_capital_effect = yes
			}
			change_tag = A79
			remove_non_electors_emperors_from_empire_effect = yes
			if = {
				limit = {
					NOT = { government_rank = 3 }
				}
				set_government_rank = 3
			}
			dragon_coast_region = {
				limit = {
					NOT = { owned_by = ROOT }
				}
				add_permanent_claim = A79
			}
			add_prestige = 25
			add_country_modifier = {
				name = "centralization_modifier"
				duration = 7300
			}
			set_country_flag = formed_gnomish_hierarchy_flag
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	

	iochand_nation = {
			major = yes
			potential = {
				normal_or_historical_nations = yes
				NOT = { has_country_flag = formed_iochand_flag }
				OR = {
					ai = no
					is_playing_custom_nation = no
				}
				NOT = { exists = A80 }
				primary_culture = creek_gnome
				OR = {
					ai = no
					AND = {
						ai = yes
						num_of_cities = 3
					}
				}
				is_colonial_nation = no
				OR = {
					is_former_colonial_nation = no
					AND = {
						is_former_colonial_nation = yes
						ai = no
					}
				}
			}
			allow = {
				is_free_or_tributary_trigger = yes
				is_nomad = no
				is_at_war = no
				owns_core_province = 142		# Iochand
			}
			effect = {
				142 = {	#Move capital to Oddansbay
					move_capital_effect = yes
					#add_base_tax = 2
					#add_base_production = 2
					#add_base_manpower = 1
				}
				change_tag = A80
				remove_non_electors_emperors_from_empire_effect = yes
				if = {
					limit = {
						NOT = { government_rank = 2 }
					}
					set_government_rank = 2
				}
				iochand_area = {
					limit = {
						NOT = { owned_by = ROOT }
					}
					add_permanent_claim = A80
				}
				add_prestige = 10
				add_country_modifier = {
					name = "centralization_modifier"
					duration = 7300
				}
				
				define_ruler = {
					dynasty = "s�l Iochand"
					adm = 3
					adm = 3
					adm = 3
				}
				add_legitimacy = 20
				if = {
					limit = {
						has_heir = yes
					}
					remove_heir = yes
				}
				define_heir = {
					dynasty = "s�l Iochand"
				}
	
				set_country_flag = formed_iochand_flag
			}
			ai_will_do = {
				factor = 1
			}
			ai_importance = 400
		}
}