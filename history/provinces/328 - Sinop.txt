#328 - Sinope

owner = A42
controller = A42
add_core = A42
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 7
base_production = 7
base_manpower = 7

trade_goods = gold

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish