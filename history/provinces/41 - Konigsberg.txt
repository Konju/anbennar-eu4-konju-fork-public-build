#41 - Konigsberg | New Pearlywine

owner = A11
controller = A11
add_core = A11
culture = pearlsedger
religion = regent_court

hre = yes

base_tax = 6
base_production = 6
base_manpower = 4

trade_goods = wine

capital = "of Pearl Modern fame"

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
