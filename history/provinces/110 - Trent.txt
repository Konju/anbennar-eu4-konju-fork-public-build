#110 - Trent

owner = A02
controller = A02
add_core = A02
culture = derannic
religion = regent_court
base_tax = 5
base_production = 5
trade_goods = naval_supplies
base_manpower = 3
capital = ""
estate = estate_nobles
is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
