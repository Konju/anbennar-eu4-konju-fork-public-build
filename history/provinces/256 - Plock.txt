#256 - Plock

owner = A46
controller = A46
add_core = A46
culture = moon_elf
religion = regent_court

hre = yes

base_tax = 6
base_production = 6
base_manpower = 4

trade_goods = glass

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish

add_permanent_province_modifier = {
	name = human_minority_integrated_large
	duration = -1
}