# No previous file for Zacatula

owner = B25
controller = B25
add_core = B25
culture = green_orc
religion = great_dookan
capital = ""

hre = no

base_tax = 2
base_production = 2
base_manpower = 1

trade_goods = unknown

native_size = 43
native_ferocity = 8
native_hostileness = 8