#Dalaskogen, northwesten part of Dalarna, including Mora and entire Siljan area.

owner = A38
controller = A38
add_core = A38
culture = anbenncoster
religion = regent_court

hre = yes

base_tax = 18
base_production = 17
base_manpower = 18

trade_goods = paper

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
discovered_by = tech_gnollish

add_permanent_province_modifier = {
	name = center_of_trade_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = elven_minority_integrated_large
	duration = -1
}

add_permanent_province_modifier = {
	name = dwarven_minority_integrated_large
	duration = -1
}

add_permanent_province_modifier = {
	name = gnomish_minority_integrated_small
	duration = -1
}

add_permanent_province_modifier = {
	name = halfling_minority_integrated_large
	duration = -1
}