#915 - Illinois

owner = A54
controller = A54
add_core = A54
culture = crownsman
religion = regent_court

hre = yes

base_tax = 8
base_production = 8
base_manpower = 6

trade_goods = cloth

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
