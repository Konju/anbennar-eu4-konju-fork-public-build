#Sönderjylland-Slesvig | Tooths End

owner = A04
controller = A04
add_core = A04
culture = west_damerian
religion = regent_court
hre = yes
base_tax = 5
base_production = 5
trade_goods = fish
base_manpower = 5
capital = ""
is_city = yes
fort_15th = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

