#347 - Ifni | 

owner = A76
controller = A76
add_core = A76
culture = uelairey
religion = regent_court

hre = yes

base_tax = 4
base_production = 3
base_manpower = 2

trade_goods = cloth

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_kobold