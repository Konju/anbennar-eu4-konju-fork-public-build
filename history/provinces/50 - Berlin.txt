# NB: If changing the ID for this province, code (CPowerSpending::OnExecute) also has to change for achievement_tear_down_this_wall to work.
#50 - Berlin | 

owner = A82
controller = A82
add_core = A82
culture = vernman
religion = regent_court
hre = yes

base_tax = 5
base_production = 5
base_manpower = 6

trade_goods = wine

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish