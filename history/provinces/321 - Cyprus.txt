#321 - Cyprus

owner = A75
controller = A75
add_core = A75
culture = arannese
religion = regent_court

hre = yes

base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
