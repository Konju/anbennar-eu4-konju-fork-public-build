government = feudal_monarchy
government_rank = 1
primary_culture = vernman
religion = regent_court
technology_group = tech_cannorian
capital = 292 # Verne
national_focus = DIP

1422.1.1 = { set_country_flag = lilac_wars_moon_party }

1443.3.3 = {
	monarch = {
		name = "Alvar IV"
		dynasty = "s�l Verne"
		birth_date = 1411.2.3
		adm = 6
		dip = 5
		mil = 5
	}
	heir = {
		name = "Dustin"
		monarch_name = "Dustin VI"
		dynasty = "s�l Verne"
		birth_date = 1430.2.27
		death_date = 1480.1.1
		claim = 95
		adm = 2
		dip = 4
		mil = 1
	}
}