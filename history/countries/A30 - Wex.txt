government = feudal_monarchy
government_rank = 2
primary_culture = wexonard
religion = regent_court
technology_group = tech_cannorian
capital = 306
national_focus = DIP
historical_rival = A13 #Gawed

1417.1.10 = {
	monarch = {
		name = "Lothane III"
		dynasty = "s�l Wex"
		birth_date = 1401.9.4
		adm = 3
		dip = 3
		mil = 6
	}
	add_ruler_personality = conqueror_personality
	queen = {
		country_of_origin = A01
		name = "Eil�sa"
		dynasty = "Siloriel"
		birth_date = 1413.4.1
		death_date = 1492.6.8
		female = yes
		adm = 1
		dip = 2
		mil = 1
	}
	add_queen_personality = fertile_personality
}

1422.1.1 = { set_country_flag = lilac_wars_rose_party }

1430.9.2 = {
	heir = {
		name = "Caylen"
		monarch_name = "Caylen II"
		dynasty = "s�l Wex"
		birth_date = 1430.9.2
		death_date = 1480.1.1
		claim = 95
		adm = 2
		dip = 2
		mil = 1
	}
}