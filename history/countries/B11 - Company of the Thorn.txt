government = adventurer
government_rank = 1
primary_culture = roilsardi
religion = regent_court
technology_group = tech_cannorian
capital = 257

1443.2.2 = {
	monarch = {
		name = "Lucian"
		dynasty = "s�l na Toars"
		birth_date = 1417.11.4
		adm = 2
		dip = 2
		mil = 6
	}
	add_ruler_personality = cruel_personality
}