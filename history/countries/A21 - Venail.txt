government = feudal_monarchy
government_rank = 1
primary_culture = moon_elf
religion = regent_court
technology_group = tech_elven
capital = 93
national_focus = DIP

1422.1.1 = { set_country_flag = lilac_wars_rose_party }

1443.2.2 = {
	monarch = {
		name = "Aldarian I"
		dynasty = Seawatcher
		birth_date = 1144.8.1
		adm = 1
		dip = 5
		mil = 1
	}
	add_ruler_personality = immortal_personality
	add_ruler_personality = navigator_personality
	add_ruler_personality = naive_personality
	set_ruler_flag = set_immortality
}